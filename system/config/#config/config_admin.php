<?php
// HTTP
define('HTTP_SERVER', 'domucmayingiare.dev/adminsmt/');
define('HTTP_CATALOG', 'domucmayingiare.dev/');
define('HTTP_IMAGE', 'domucmayingiare.dev/image/');

// HTTPS
define('HTTPS_SERVER', '');
define('HTTPS_IMAGE', '');

// DIR
define('DIR', 'D:\xampp\htdocs\domucmayingiare\\'); 

define('DIR_APPLICATION', DIR . 'adminsmt/');
define('DIR_SYSTEM', DIR . 'system/');
define('DIR_DATABASE', DIR_SYSTEM . 'database/');
define('DIR_LANGUAGE', DIR . 'adminsmt/language/');
define('DIR_TEMPLATE', DIR . 'adminsmt/view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_IMAGE', DIR . 'image/');
define('DIR_CACHE', DIR_SYSTEM . 'cache/');
define('DIR_DOWNLOAD', DIR . 'download/');
define('DIR_LOGS', DIR_SYSTEM . 'logs/');
define('DIR_CATALOG', DIR . 'catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'domucmayingiare');
define('DB_PREFIX', '');
?>
